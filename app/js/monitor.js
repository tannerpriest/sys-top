const path = require("path")
const osu = require("node-os-utils")
const { ipcRenderer, app } = require("electron")

const cpu = osu.cpu
const mem = osu.mem
const os = osu.os

let cpuOverload;
let alertFrequency;

ipcRenderer.on("settings:get", (e, settings) => {
    cpuOverload = +settings.cpuOverload
    alertFrequency = +settings.alertFrequency
    // clear/reset interval for notification when the settings has been updated
    localStorage.removeItem("lastNotify")
})

const sendAlert = (options) => {
    ipcRenderer.send("notify:overload", options)
}

let lastNotify = "lastNotify"

const useNotify = (frequency) => {
    if (localStorage.getItem(lastNotify) === null) {
        return true
    }
    const notifyTime = new Date(parseInt(localStorage.getItem(lastNotify)))
    const now = new Date()
    const diffTime = Math.abs(now - notifyTime)

    const minutesPassed = Math.ceil(diffTime / (1000 * 60))

    if (minutesPassed > frequency) {
        return true
    }

    return false
}

setInterval(() => {
    // cpu usage
    cpu.usage().then(info => {
        document.getElementById("cpu-usage").innerText = info.toFixed(2) + "%"

        document.getElementById("cpu-progress").style.width = info.toFixed(2) + "%"

        // make progress red if overload
        if (info >= cpuOverload) {
            document.getElementById("cpu-progress").style.background = "red"
        } else {
            document.getElementById("cpu-progress").style.background = "#30c88b"
        }

        if (info >= cpuOverload && useNotify(alertFrequency)) {
            sendAlert({
                title: "CPU OVERLOAD",
                body: `CPU IS OVER ${cpuOverload}%`,
                icon: path.join(__dirname, "img", "icon.png"),
            })
            localStorage.setItem(lastNotify, +new Date())
        }
    })
    cpu.free().then(info => {
        document.getElementById("cpu-free").innerText = info.toFixed(2) + "%"
    })


    document.getElementById("sys-uptime").innerText = setSecondsToDhms(os.uptime())

}, 2000)

// set model
document.getElementById("cpu-model").innerText = cpu.model()

// set comp name
document.getElementById("comp-name").innerText = os.hostname()

// set OS
document.getElementById("os").innerText = `${os.type()} ${os.arch()}`

//total memory
mem.info().then(info => document.getElementById("mem-total").innerText = `${info.totalMemMb} B - ${(info.totalMemMb / 1024)} GB`)

const setSecondsToDhms = (seconds) => {
    seconds = +seconds
    const secondsInADay = 3600 * 24
    const d = Math.floor(seconds / (secondsInADay))
    const h = Math.floor((seconds % (secondsInADay)) / 3600)
    const m = Math.floor((seconds % 3600) / 60)
    const s = Math.floor((seconds % 60))

    return `${d}d, ${h}h, ${m}m, ${s}s, `
}
