const Store = require("./Store");
const path = require("path")
const { app, BrowserWindow, Menu, ipcMain, MenuItem, Notification } = require('electron');
const MainWindow = require("./MainWindow");
const AppTray = require("./AppTray");

// Set env
process.env.NODE_ENV = 'development'

const isDev = process.env.NODE_ENV !== 'production' ? true : false
const isMac = process.platform === 'darwin' ? true : false

/** 
* @type {BrowserWindow}
*/

let mainWindow;
let tray;

const store = new Store({
  configName: "user-settings",
  defaults: {
    settings: {
      cpuOverload: 80,
      alertFrequency: 5
    }
  }
})

function createMainWindow() {
  mainWindow = new MainWindow('./app/index.html', isDev)
}

app.on('ready', () => {
  createMainWindow()
  mainWindow.webContents.on("dom-ready", () => {
    mainWindow.webContents.send("settings:get", store.get("settings"))
  })

  const mainMenu = Menu.buildFromTemplate(menu)
  Menu.setApplicationMenu(mainMenu)

  mainWindow.on("close", (e) => {
    if (!app.isQuitting) {
      e.preventDefault()
      mainWindow.hide()
    }
  })

  const icon = path.join(__dirname, "assets/icons/tray_icon.png")

  // create tray
  tray = new AppTray(icon, mainWindow)

})

/**
 * @type {MenuItem[]}
*/

const menu = [
  ...(isMac ? [{ role: 'appMenu' }] : []),
  {
    role: 'fileMenu',
  },
  {
    label: "View",
    submenu: [
      {
        label: "Toggle Navigation",
        click: () => mainWindow.webContents.send("nav:toggle")
      }
    ]
  },
  ...(isDev
    ? [
      {
        label: 'Developer',
        submenu: [
          { role: 'reload' },
          { role: 'forcereload' },
          { type: 'separator' },
          { role: 'toggledevtools' },
        ],
      },
    ]
    : []),
]

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow()
  }
})

app.allowRendererProcessReuse = true

// set settings
ipcMain.on("settings:set", (e, settings) => {
  store.set("settings", settings)
  mainWindow.webContents.send("settings:get", store.get("settings"))
})

ipcMain.on("window:show", (e, settings) => {
  mainWindow.show()
})

app.on("activate", () => {
  mainWindow.show()
})

ipcMain.on("notify:overload", (e,options) => {
  let warning = new Notification(options)
  warning.on("click", ()=> mainWindow.show())
  warning.show()
})