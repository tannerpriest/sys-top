const electron  = require("electron")
const path = require("path")
const fs = require("fs")

const parseDataFile = (filePath, defaults) => {
    try {
        // if json file exists, load the written settings
        return JSON.parse(fs.readFileSync(filePath))
    } catch (error) {
        // return the defaults that was defined with the class was built
        console.error(error)
        return defaults
    }
}

class Store {
    constructor(options) {
        const userDataPath = (electron.app.getPath("userData") || electron.remote.app.getPath("userData"))

        this.path = path.join(userDataPath, options.configName + ".json")

        this.data = parseDataFile(this.path, options.defaults)
    }

    get(key){
        return this.data[key]
    }

    set(key, val ){
        this.data[key] = val
        fs.writeFileSync(this.path, JSON.stringify(this.data))
    }
}

module.exports = Store